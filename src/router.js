const express = require('express');
const router = express();

//router endpoint
router.get("/", (req, res) => res.send("Welcome!"));

// router endpoint to add two numbers together. Query parameters.
router.get("/add", (req, res) => {
    try {
        const sum = req.query.a + req.query.b;
        res.send(sum.toString());
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get("/multiply", (req, res) => {
    try {
        const multiply = req.query.a + req.query.b;
        res.send(multiply.toString());
    } catch (e) {
        res.sendStatus(500);
    }
});

//Endpoint 3 POST http://localhost:3000/webhook-update
/**Proper JSON REST API Middlewares */
router.use(express.urlencoded({ extended: true }));
router.use(express.json());
// post here

module.exports = router;